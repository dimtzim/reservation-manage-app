package com.dtzimoulidis.rateboard;

import com.dtzimoulidis.rateboard.controller.ReservationController;
import com.dtzimoulidis.rateboard.event.ReservationHandlingEvent;
import com.dtzimoulidis.rateboard.model.Reservation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RateboardApplicationTests {
	@Autowired
	private MockMvc mockMvc;
	@Mock
	BlockingQueue<Reservation> reservationQueue;
	@Mock
	private ApplicationEventPublisher publisher;
	@Captor
	private ArgumentCaptor<ReservationHandlingEvent> captor;
	@InjectMocks
	private ReservationController reservationController;

	@Test
	void shouldBeOk() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders.post("/reservation")
				.contentType(MediaType.APPLICATION_JSON)
				.content(createReservationRequestBody()))
				.andExpect(status().isOk());

	}

	@Test
	void shouldReturnReservation() {
		Reservation reservation = createReservation();
		reservationController.produce(reservation);
		verify(publisher, times(1)).publishEvent(captor.capture());
		assertEquals(captor.getValue().getReservation().getMessage(), reservation.getMessage());
	}

	private String createReservationRequestBody() {
		Reservation reservation = new Reservation();
		reservation.setMessage("Test");
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = null;
		try {
			requestJson=ow.writeValueAsString(reservation);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return requestJson;
	}

	private Reservation createReservation() {
		Reservation reservation = new Reservation();
		reservation.setMessage("Test");
		return reservation;
	}


}
