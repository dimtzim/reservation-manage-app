package com.dtzimoulidis.rateboard.config;

import com.dtzimoulidis.rateboard.model.FIFOEntry;
import com.dtzimoulidis.rateboard.model.Reservation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.PriorityBlockingQueue;

@Configuration
public class QueueConfig {
    @Bean
    public BlockingQueue<FIFOEntry<Reservation>> reservationQueue() {
        return new PriorityBlockingQueue<>();
    }

    @Bean
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(15);
        executor.setThreadNamePrefix("Reservation-Thread");
        executor.initialize();
        return executor;
    }
}
