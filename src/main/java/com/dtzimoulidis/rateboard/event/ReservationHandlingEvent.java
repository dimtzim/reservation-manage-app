package com.dtzimoulidis.rateboard.event;

import com.dtzimoulidis.rateboard.model.Reservation;
import lombok.Getter;

@Getter
public class ReservationHandlingEvent {
    private Reservation reservation;

    public ReservationHandlingEvent(final Reservation reservation) {
        this.reservation = reservation;
    }

    @Override
    public String toString() {
        return "Reservation Handled: " + reservation.getMessage() ;
    }
}
