package com.dtzimoulidis.rateboard.service;

import com.dtzimoulidis.rateboard.model.Reservation;

public interface ReservationService {
    String getReservationMessage(final Reservation reservation);
}
