package com.dtzimoulidis.rateboard.service.impl;

import com.dtzimoulidis.rateboard.model.Reservation;
import com.dtzimoulidis.rateboard.service.ReservationService;
import org.springframework.stereotype.Service;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Override
    public String getReservationMessage(Reservation reservation) {
        return reservation.getMessage();
    }
}
