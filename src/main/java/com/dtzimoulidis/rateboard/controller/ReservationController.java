package com.dtzimoulidis.rateboard.controller;

import com.dtzimoulidis.rateboard.event.ReservationHandlingEvent;
import com.dtzimoulidis.rateboard.model.FIFOEntry;
import com.dtzimoulidis.rateboard.model.Reservation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.concurrent.BlockingQueue;

@RestController
public class ReservationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationController.class);

    private final BlockingQueue<FIFOEntry<Reservation>> reservationQueue;
    private final ApplicationEventPublisher publisher;

    @Autowired
    public ReservationController(final BlockingQueue<FIFOEntry<Reservation>> reservationQueue,
                                 final ApplicationEventPublisher publisher) {
        this.reservationQueue = reservationQueue;
        this.publisher = publisher;
    }

    @PostMapping(path = "/reservation")
    public void produce(@Valid @RequestBody Reservation reservation) {
        try {
            reservationQueue.put(new FIFOEntry(reservation));
            LOGGER.info("Reservation Received: {}" ,reservation.getMessage());
            publisher.publishEvent(new ReservationHandlingEvent(reservation));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
