package com.dtzimoulidis.rateboard.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode
public class Reservation implements Comparable<Reservation>{
    @NotNull
    private String message;
    private boolean priority;

    @Override
    public int compareTo(Reservation o) {
        if (this.isPriority() && !o.isPriority()) {
            return -1;
        } else if (!this.isPriority() && o.isPriority()) {
            return 1;
        } else {
            return 0;
        }
    }

}
