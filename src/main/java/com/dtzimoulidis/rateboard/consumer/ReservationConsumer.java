package com.dtzimoulidis.rateboard.consumer;

import com.dtzimoulidis.rateboard.event.ReservationHandlingEvent;
import com.dtzimoulidis.rateboard.model.FIFOEntry;
import com.dtzimoulidis.rateboard.model.Reservation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;

@Component
public class ReservationConsumer{
    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationConsumer.class);

    private final BlockingQueue<FIFOEntry<Reservation>> reservationQueue;

    @Autowired
    public ReservationConsumer(final BlockingQueue<FIFOEntry<Reservation>> reservationQueue) {
        this.reservationQueue = reservationQueue;
    }

    @Async
    @EventListener
    public void handleAsync(ReservationHandlingEvent event) {
        try {
            Thread.sleep(10000);
            FIFOEntry<Reservation> reservation = reservationQueue.take();
            LOGGER.info("Reservation processed {}", reservation.getEntry().getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
