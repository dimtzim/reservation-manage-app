# Queue Reservation App

### How to run
use `mvn spring-boot:run` to run the application

### HTTP Web service for imporing reservation
``POST /reservation``

 - body: ```{
    "message":"reservation message",
    "priority": true/false
    }```
    
 message - is required, 
 priority - is by default false if not specified. 
 all the rest of the incoming reservations are treated in FIFO principal
 
### Design decisions

- PriorityBlockingQueue, we are using this data structure because is thread-safe 
as we will have multiple producers(multiple hosts calling /reservation) and we want to
deal with concurrency. Also, unlike other BlockingQueues, PriorityBlockingQueue is an unbound queue
which means that does not have a maximum capacity, so the produces will not be blocked when adding reservations
to the queue
    - Memory and efficiency wise, the PriorityBlockingQueue is very memory efficient as it uses an array-based binary tree heap
    
### Issues

The issues that may occur, come with the unbounded part of this data structure.
It's capacity grows dynamically.

